#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

void Jeu::raz() {
    for(auto & l : _plateau)
        for(auto & c : l)
            c = JOUEUR_VIDE;
}

Joueur Jeu::getVainqueur() const {
    return JOUEUR_VIDE;
}

Joueur Jeu::getJoueurCourant() const {
    return courant;
    //TODO
}

bool Jeu::jouer(int i, int j) {
    if(getPlateau(i, j) != JOUEUR_VIDE){
        _plateau[i][j] = getJoueurCourant();
    }
    return false;
}

 Joueur Jeu::getPlateau(int i, int j) const {
     return _plateau[i][j];
 }

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    for (auto & l : jeu._plateau)
    {
        for (auto & c : l)
        {
            if (c == JOUEUR_ROUGE)
            {
                os << 'R';
            }
            else if (c == JOUEUR_VERT)
            {
                os << 'V';
            }
            else 
            {
                os << '.';
            }
        }
        os <<std::endl;
    }
    return os;
}