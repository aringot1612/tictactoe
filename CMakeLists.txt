cmake_minimum_required( VERSION 3.0 )

project( tictactoe )

add_executable( tictactoe-cli cpp/tictactoe-cli.cpp cpp/Tictactoe.cpp )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKGCPPUTEST REQUIRED cpputest )
include_directories( ${PKGCPPUTEST_INCLUDE_DIRS} )
add_executable( tictactoe-test cpp/tictactoe-test.cpp cpp/Tictactoe.cpp cpp/TictactoeTest.cpp )
target_link_libraries( tictactoe-test ${PKGCPPUTEST_LIBRARIES} )

install( TARGETS tictactoe-cli DESTINATION bin )

